import 'package:flutter/material.dart';
import 'package:food_odering_app/providers/provider_product.dart';
import 'package:food_odering_app/screens/screen_home.dart';
import 'package:food_odering_app/screens/screen_initial.dart';
import 'package:food_odering_app/screens/screen_menu_detailed.dart';
import 'package:provider/provider.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider.value(value: ProviderProduct()),
      ],
      child: MaterialApp(
        title: 'Flutter Demo',
        theme: ThemeData(
          primarySwatch: Colors.red,
        ),
        routes: {
          '/': (ctx) => AppInitialScreen(),
          ScreenHome.routeName:(ctx)=>ScreenHome(),
          ScreenMenuDetailed.routeName:(ctx)=>ScreenMenuDetailed()
        },
      ),
    );
  }
}
