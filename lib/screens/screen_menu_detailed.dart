import 'package:flutter/material.dart';
import 'package:food_odering_app/models/model_product.dart';
import 'package:food_odering_app/providers/provider_product.dart';
import 'package:provider/provider.dart';

class ScreenMenuDetailed extends StatelessWidget {
  static final routeName = 'screen_menu_detailed';
  var context;
  ModelSingleProduct productItem;

  @override
  Widget build(BuildContext context) {
    this.context = context;
    final itemId = ModalRoute.of(context).settings.arguments as String;
    productItem = Provider.of<ProviderProduct>(context).getProductById(itemId);
    return Scaffold(
        body: Stack(
      children: <Widget>[
        _imageBackGround(),
        _detailedItem(),
      ],
    ));
  }

  Widget _imageBackGround() {
    return Container(
      width: double.infinity,
      height: double.infinity,
      child: Image.network(
        productItem.imageUrl,
        fit: BoxFit.cover,
      ),
    );
  }

  Widget _detailedItem() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Container(
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(
              topRight: Radius.circular(30),
              topLeft: Radius.circular(30),
            ),
          ),
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                _itemTitie(productItem.itemName,productItem.id),
                SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    _rating(productItem.ratings),
                    SizedBox(
                      width: 15,
                    ),
                    _timeToDeliver(productItem.preparationTime),
                    SizedBox(
                      width: 15,
                    ),
                    _category(productItem.category)
                  ],
                ),
                SizedBox(
                  height: 10,
                ),
                _description(productItem.description),
                SizedBox(
                  height: 10,
                ),
                _amountAndPrice(productItem.price[0]),
                SizedBox(
                  height: 10,
                ),
                _amountAndPrice(productItem.price[1]),
                SizedBox(
                  height: 20,
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  Widget _itemTitie(String value,String id) {
    return Hero(
      tag: id,
      child: Text(
        value,
        style: TextStyle(fontWeight: FontWeight.w800, fontSize: 25),
      ),
    );
  }

  Widget _rating(String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.star,
          color: Colors.yellow,
          size: 20,
        ),
        Text(value)
      ],
    );
  }

  Widget _timeToDeliver(String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.access_time,
          size: 15,
        ),
        Text(value)
      ],
    );
  }

  Widget _category(String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.error_outline,
          size: 15,
        ),
        Text(value)
      ],
    );
  }

  Widget _description(String value) {
    return Container(
      width: double.infinity,
      child: Text(value,
          style: TextStyle(color: Colors.grey),
          maxLines: 5,
          textAlign: TextAlign.left,
          softWrap: true,
          overflow: TextOverflow.ellipsis),
    );
  }

  Widget _amountAndPrice(ModelPrice price) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        _price(price),
        _addToCartButton(),
      ],
    );
  }

  Widget _price(ModelPrice price) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.monetization_on,
          color: Colors.grey,
          size: 30,
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          price.price.toString(),
          style: TextStyle(fontSize: 23, fontWeight: FontWeight.w300),
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          '|',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
        ),
        SizedBox(
          width: 10,
        ),
        Text(
          '${price.amount.toString()} g',
          style: TextStyle(fontSize: 20, fontWeight: FontWeight.w300),
        ),
      ],
    );
  }

  Widget _addToCartButton() {
    return Container(
      height: 35,
      child: RaisedButton(
        child: Text(
          'Add to cart',
          style: TextStyle(color: Colors.white),
        ),
        onPressed: () {},
        color: Theme.of(context).primaryColor,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(5.0),
            side: BorderSide(color: Colors.red)),
      ),
    );
  }
}
