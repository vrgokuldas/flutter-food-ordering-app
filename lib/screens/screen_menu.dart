import 'package:flutter/material.dart';
import 'package:food_odering_app/providers/provider_product.dart';
import 'package:food_odering_app/screens/screen_menu_detailed.dart';
import 'package:food_odering_app/static_class/constants.dart';
import 'package:food_odering_app/widgets/ui_item_menu.dart';
import 'package:provider/provider.dart';

class ScreenMenu extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final ProductDB= Provider.of<ProviderProduct>(context);
    final productListCount = ProductDB.itemCount;
    final productList = ProductDB.productList;
    return Scaffold(
      body: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 225,
                child: Image.network(
                  ImageUrl.saladImage,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                width: double.infinity,
                height: 225,
                color: Colors.black26,
                child: Padding(
                  padding: const EdgeInsets.all(20),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: <Widget>[
                      Text(
                        'Salads',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 30,
                            fontWeight: FontWeight.w600),
                      ),
                      Text(
                        '48 Items',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 15,
                            fontWeight: FontWeight.w400),
                      )
                    ],
                  ),
                ),
              ),
            ],
          ),
          Expanded(
            child: ListView.builder(
                itemCount: productListCount,
                itemBuilder: (ctx, index) {
                  return UiItemMenu(productList[index].id);
                }),
          )
        ],
      ),
    );
  }
}
