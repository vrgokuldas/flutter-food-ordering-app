import 'package:flutter/material.dart';
import 'package:food_odering_app/screens/screen_home.dart';
import 'package:food_odering_app/static_class/constants.dart';

class AppInitialScreen extends StatelessWidget {
  final textColor = Colors.white;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          height: double.infinity,
          width: double.infinity,
          child: Stack(
            children: <Widget>[
              Container(
                width: double.infinity,
                height: double.infinity,
                child: Image.network(
                  ImageUrl.signInImage1,
                  fit: BoxFit.cover,
                ),
              ),
              Container(
                color: Colors.black54,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        'Food App Concept',
                        style: TextStyle(color: textColor),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Welcome!',
                        style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 40,
                            color: textColor),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        'Login to your account or choose the city for being served without login',
                        style: TextStyle(color: textColor),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: double.infinity,
                        height: 50,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.white),
                        child: RaisedButton(
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text('Please, choose your town'),
                              Icon(
                                Icons.expand_more,
                                size: 30,
                              )
                            ],
                          ),
                          onPressed: () {},
                          color: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        width: double.infinity,
                        height: 50,
                        color: Colors.transparent,
                        child: RaisedButton(
                          child: Text(
                            'Login to Account',
                            style: TextStyle(fontSize: 15),
                          ),
                          onPressed: (){
                            Navigator.of(context).pushNamed(ScreenHome.routeName);
                          },
                          color: Theme.of(context).primaryColor,
                          textColor: Colors.white,
                        ),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                    ],
                  ),
                ),
              )
            ],
          )),
    );
  }
}
