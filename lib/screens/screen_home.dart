import 'package:flutter/material.dart';
import 'package:food_odering_app/screens/screen_cart.dart';
import 'package:food_odering_app/screens/screen_menu.dart';
import 'package:food_odering_app/screens/screen_orders.dart';
import 'package:food_odering_app/screens/screen_profile.dart';

class ScreenHome extends StatefulWidget {
  static final routeName = 'screen_home';

  @override
  _ScreenHomeState createState() => _ScreenHomeState();
}

class _ScreenHomeState extends State<ScreenHome> {
  var _selectedIndex = 0;

  final pages = [
    ScreenMenu(),
    ScreenCart(),
    ScreenOrders(),
    ScreenProfile(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[_selectedIndex],
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: _selectedIndex,
        type: BottomNavigationBarType.fixed,
        onTap: _changeIndex,
        items: [
          BottomNavigationBarItem(
              icon: Icon(Icons.dashboard), title: Text('Menu')),
          BottomNavigationBarItem(
              icon: Icon(Icons.shopping_cart), title: Text('Cart')),
          BottomNavigationBarItem(
              icon: Icon(Icons.format_list_bulleted), title: Text('Orders')),
          BottomNavigationBarItem(
              icon: Icon(Icons.person), title: Text('Profile')),
        ],
      ),
    );
  }

  void _changeIndex(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }
}
