import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:food_odering_app/models/model_product.dart';

class ProviderProduct with ChangeNotifier {
  var _jsonString = [
    {
      "id": "1",
      "item_name": "Cezarre Original",
      "ratings": "4.9",
      "preparation_time": "15 mins",
      "category": "Cheese",
      "price": [
        {"price": 136, "amount": 230},
        {"price": 172, "amount": 340}
      ],
      "description":
          "Cheesy mayo sauce and mozarella, tomatoes, green peper, onion",
      "imageUrl":
          "https://i.pinimg.com/originals/f0/b6/15/f0b615f78dd809d68ec389f4bc8d94bb.jpg"
    },
    {
      "id": "2",
      "item_name": "Beetroot & walnut salad",
      "ratings": "4.8",
      "preparation_time": "25 mins",
      "category": "Vegan",
      "price": [
        {"price": 108, "amount": 230},
        {"price": 216, "amount": 340}
      ],
      "description":
          "Baby beetroot, olive oil, goat's cheese, redwine vinegar, rocket leaves",
      "imageUrl":
          "https://previews.123rf.com/images/draghicich/draghicich1309/draghicich130900051/22306419-still-life-with-turkish-doner-kebab-and-shawarma.jpg"
    },
    {
      "id": "3",
      "item_name": "Beans & chicken salad",
      "ratings": "4.8",
      "preparation_time": "15 mins",
      "category": "Fruits",
      "price": [
        {"price": 136, "amount": 230},
        {"price": 272, "amount": 340}
      ],
      "description":
          "Dry thyme, chicken breast, fresh pinapple, brown rice, black beans",
      "imageUrl":
          "https://webtafri.com/wp-content/uploads/2017/10/non-veg-biryani.jpg"
    }
  ];

  ProductList parseJsonToClasses() => ProductList.fromJson(_jsonString);

  List<ModelSingleProduct> get productList {
    return [...parseJsonToClasses().productList];
  }

  ModelSingleProduct getProductById(String id)
  {
   return parseJsonToClasses().productList.firstWhere((item)=>item.id == id);
  }

  int get itemCount{
    return parseJsonToClasses().productList.length;
  }
}
