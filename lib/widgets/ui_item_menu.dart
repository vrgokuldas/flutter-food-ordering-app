import 'package:flutter/material.dart';
import 'package:food_odering_app/providers/provider_product.dart';
import 'package:food_odering_app/screens/screen_menu_detailed.dart';
import 'package:provider/provider.dart';

class UiItemMenu extends StatelessWidget {
  final itemId;
  UiItemMenu(this.itemId);

  @override
  Widget build(BuildContext context) {
    final productItem=Provider.of<ProviderProduct>(context).getProductById(itemId);
    return Card(
      child: InkWell(
        onTap: () {
          Navigator.of(context)
              .pushNamed(ScreenMenuDetailed.routeName,arguments: itemId);
        },
        child: Container(
          width: double.infinity,
          padding: const EdgeInsets.all(10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              _itemImage(productItem.imageUrl),
              SizedBox(width: 15),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  _itemTitie(productItem.itemName,productItem.id),
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      _rating(productItem.ratings),
                      SizedBox(width: 15),
                      _timeToDeliver(productItem.preparationTime),
                      SizedBox(width: 15),
                      _category(productItem.category),
                    ],
                  ),
                  SizedBox(height: 5),
                  _description(productItem.description),
                  SizedBox(height: 5),
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      _price(productItem.price[0].price.toString()),
                      SizedBox(
                        width: 50,
                      ),
                      _addToCartButton(context),
                    ],
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }

  Widget _itemImage(String imageUrl) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Container(
          width: 100,
          height: 100,
          child: Image.network(
            imageUrl,
            fit: BoxFit.cover,
          )),
    );
  }

  Widget _itemTitie(String title,String id) {
    return Hero(
      tag: id,
      child: Text(
        title,
        style: TextStyle(fontWeight: FontWeight.w800, fontSize: 17),
      ),
    );
  }

  Widget _rating(String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.star,
          color: Colors.yellow,
          size: 20,
        ),
        Text(value)
      ],
    );
  }

  Widget _timeToDeliver(String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.access_time,
          size: 15,
        ),
        Text(value)
      ],
    );
  }

  Widget _category(String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.error_outline,
          size: 15,
        ),
        Text(value)
      ],
    );
  }

  Widget _description(String value) {
    return Container(
      width: 200,
      child: Text(
          value,
          style: TextStyle(color: Colors.grey),
          maxLines: 2,
          textAlign: TextAlign.left,
          softWrap: true,
          overflow: TextOverflow.ellipsis),
    );
  }

  Widget _price(String value) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        Icon(
          Icons.monetization_on,
          color: Colors.black,
          size: 20,
        ),
        Text(value)
      ],
    );
  }

  Widget _addToCartButton(BuildContext context) {
    return Container(
      height: 30,
      child: RaisedButton(
        child: Text(
          'Add to cart',
          style: TextStyle(color: Colors.white),
        ),
        onPressed: () {},
        color: Theme.of(context).primaryColor,
        shape: RoundedRectangleBorder(
            borderRadius: new BorderRadius.circular(5.0),
            side: BorderSide(color: Colors.red)),
      ),
    );
  }
}
