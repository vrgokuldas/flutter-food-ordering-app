class ProductList{
  List<ModelSingleProduct> productList;

  ProductList(this.productList);

  factory ProductList.fromJson(List<dynamic> jsonString)
  {
    List<ModelSingleProduct> products = new List<ModelSingleProduct>();
    products= jsonString.map((i)=>ModelSingleProduct.fromJson(i)).toList();
    return ProductList(products);
  }
}


class ModelSingleProduct {
  String _id;
  String _itemName;
  String _ratings;
  String _preparationTime;
  String _category;
  List<ModelPrice> _price;
  String _description;
  String _imageUrl;

  ModelSingleProduct(
      {String id,
        String itemName,
        String ratings,
        String preparationTime,
        String category,
        List<ModelPrice> price,
        String description,
        String imageUrl}) {
    this._id = id;
    this._itemName = itemName;
    this._ratings = ratings;
    this._preparationTime = preparationTime;
    this._category = category;
    this._price = price;
    this._description = description;
    this._imageUrl = imageUrl;
  }

  String get id => _id;
  set id(String id) => _id = id;
  String get itemName => _itemName;
  set itemName(String itemName) => _itemName = itemName;
  String get ratings => _ratings;
  set ratings(String ratings) => _ratings = ratings;
  String get preparationTime => _preparationTime;
  set preparationTime(String preparationTime) =>
      _preparationTime = preparationTime;
  String get category => _category;
  set category(String category) => _category = category;
  List<ModelPrice> get price => _price;
  set price(List<ModelPrice> price) => _price = price;
  String get description => _description;
  set description(String description) => _description = description;
  String get imageUrl => _imageUrl;
  set imageUrl(String imageUrl) => _imageUrl = imageUrl;

  ModelSingleProduct.fromJson(Map<String, dynamic> json) {
    _id = json['id'];
    _itemName = json['item_name'];
    _ratings = json['ratings'];
    _preparationTime = json['preparation_time'];
    _category = json['category'];
    if (json['price'] != null) {
      _price = new List<ModelPrice>();
      json['price'].forEach((v) {
        _price.add(new ModelPrice.fromJson(v));
      });
    }
    _description = json['description'];
    _imageUrl = json['imageUrl'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this._id;
    data['item_name'] = this._itemName;
    data['ratings'] = this._ratings;
    data['preparation_time'] = this._preparationTime;
    data['category'] = this._category;
    if (this._price != null) {
      data['price'] = this._price.map((v) => v.toJson()).toList();
    }
    data['description'] = this._description;
    data['imageUrl'] = this._imageUrl;
    return data;
  }
}

class ModelPrice {
  int _price;
  int _amount;

  ModelPrice({int price, int amount}) {
    this._price = price;
    this._amount = amount;
  }

  int get price => _price;
  set price(int price) => _price = price;
  int get amount => _amount;
  set amount(int amount) => _amount = amount;

  ModelPrice.fromJson(Map<String, dynamic> json) {
    _price = json['price'];
    _amount = json['amount'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['price'] = this._price;
    data['amount'] = this._amount;
    return data;
  }
}