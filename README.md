# food_odering_app

Flutter UI demo for Food Ordering apps

The Design is developed by Viktoriia Uhryn for EPAM Design Lviv

Visit Her Dribbble Profile

https://dribbble.com/shots/8086894-Food-Catering-App-Concept

![Scheme](https://cdn.dribbble.com/users/2249896/screenshots/8086894/media/c3c6bf41d371da8f411cb49aa8bfb026.png)

**NOTE**

(1) The Exact images are not Used, & I added some UI Changes to make it more attract
(2) Tested with iPhone 11 Max Pro
